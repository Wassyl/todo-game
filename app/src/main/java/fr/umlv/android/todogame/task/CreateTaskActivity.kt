package fr.umlv.android.todogame.task

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.*
import androidx.annotation.RequiresApi
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.appcompat.app.AppCompatActivity
import fr.umlv.android.todogame.R
import fr.umlv.android.todogame.database.DataBase
import fr.umlv.android.todogame.database.entityclass.Category
import fr.umlv.android.todogame.database.entityclass.Task

class CreateTaskActivity : AppCompatActivity(){

    private var spinner: Spinner? = null
    private var arrayAdapter: ArrayAdapter<Category>? = null
    private var category : List<Category>? = null

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_task)

        category = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            DataBase.getDataBaseInstance(applicationContext)?.getCategory();
        } else {
            TODO("VERSION.SDK_INT < P")
        }

        spinner = findViewById(R.id.etCategories)
        arrayAdapter = ArrayAdapter(this, R.layout.spinner_item_selected, category!!)
        arrayAdapter!!.setDropDownViewResource(R.layout.spinner_dropdown_item)
        spinner?.adapter = arrayAdapter

        findViewById<FloatingActionButton>(R.id.btNewQuest).setOnClickListener {
            val quest = Task(0,
                    findViewById<TextView>(R.id.etTitle).text.toString(),
                    findViewById<RatingBar>(R.id.etDifficulty).rating,
                    findViewById<TextView>(R.id.etDescription).text.toString(),
                    findViewById<Spinner>(R.id.etCategories).selectedItem.toString(),
            )
            DataBase.getDataBaseInstance(applicationContext)?.insertQuest(quest)
            val intent = Intent(this, DisplayTaskActivity::class.java)
            intent.putExtra("category", findViewById<Spinner>(R.id.etCategories).selectedItem.toString())
            startActivity(intent)
            finish()
        }
    }
}