package fr.umlv.android.todogame.dungeon;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class Chest implements Sprite {

    private final Bitmap image;
    private final int imageWidth;
    private final int imageHeight;

    private final int spriteWidth;
    private final int spriteHeight;

    private final int boundedBoxX;
    private final int boundedBoxY;

    private final Bitmap[] frames;

    private final int x;
    private final int y;
    private int currentCol = 0;

    private boolean isOpened = false;

    Chest(Bitmap image, int rowCount, int colCount, int x, int y) {
        this.image = image;

        this.imageWidth = image.getWidth();
        this.imageHeight = image.getHeight();

        this.spriteWidth = imageWidth/colCount;
        this.spriteHeight = imageHeight/rowCount;

        this.boundedBoxX = spriteWidth/2;
        this.boundedBoxY = spriteHeight/2;

        this.x = x;
        this.y = y;

        this.frames = new Bitmap[colCount];
        for( int i = 0; i < colCount; i++){
            this.frames[i] = getSubImage(0, i);
        }
    }

    private Bitmap getSubImage(int row, int col){
        return Bitmap.createBitmap(image, col*spriteWidth, row*spriteHeight, spriteWidth, spriteHeight);
    }

    public void getReward() {
        if( isOpened ) {
            return;
        }
        isOpened = true;
        currentCol = 1;

        // TODO reward
    }

    @Override
    public Rect getCollisionShape() {
        return new Rect(x - boundedBoxX, y - boundedBoxY, x + boundedBoxX, y + boundedBoxY);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawBitmap(frames[currentCol], x - boundedBoxX, y - boundedBoxY, null);
    }

    @Override
    public void updateVector(float destX, float destY) {}
}
