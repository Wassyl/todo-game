package fr.umlv.android.todogame.dungeon;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.Random;

public class GameActivity extends AppCompatActivity {

    private static final int LAUNCH_SECOND_ACTIVITY = 1;

    private int width;
    private int height;

    private GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // FullScreen mode
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // Remove Title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        width = getResources().getDisplayMetrics().widthPixels;
        height = getResources().getDisplayMetrics().heightPixels;
        gameView = new GameView(getApplicationContext(), this, width, height);
        setContentView(gameView);
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameView.resume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if(resultCode == Activity.RESULT_OK){
                Toast.makeText(this, "YOU WON !", Toast.LENGTH_LONG).show();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    //DataBase.Companion.getDataBaseInstance(getApplicationContext()).updateChestsAvailable(); // add chests
                    //DataBase.Companion.getDataBaseInstance(getApplicationContext()).updateEnemies(); // add enemies
                }

            } else {
                Toast.makeText(this, "LOST", Toast.LENGTH_LONG).show();

            }
            gameView.resume();
        }
    }

    void startMiniGame() {
        Intent miniGame = getIntentMiniGame();
        startActivityForResult(miniGame, LAUNCH_SECOND_ACTIVITY);
    }

    private Intent getIntentMiniGame() {
        int num = new Random().nextInt(3);
        switch (num) {
            case 1 : return new Intent(this, fr.umlv.android.todogame.minigames.tictactoe.GameActivity.class);
            case 2 : return new Intent(this, fr.umlv.android.todogame.minigames.memory.GameActivity.class);
            default : return new Intent(this, fr.umlv.android.todogame.minigames.doggobonkgame.MainActivity.class);
        }
    }
}
