package fr.umlv.android.todogame.minigames.tictactoe
import kotlin.math.max
import kotlin.math.min


data class Cell(val row: Int, val col: Int)

class TicTacToe {

    companion object {
        const val PLAYER = "O"
        const val COMPUTER = "X"
    }

    val g: GamePlay = GamePlay()

    var computersMove: Cell? = null

    fun placeMove(cell: Cell, player: String) {
        g.gameBoard[cell.row][cell.col] = player
    }

    fun minimax(depth: Int, player: String): Int {
        if (g.hasComputerWon()) return +1
        if (g.hasPlayerWon()) return -1

        if (g.availableCells.isEmpty()) return 0

        var min = Integer.MAX_VALUE
        var max = Integer.MIN_VALUE

        for (i in g.availableCells.indices) {
            val cell = g.availableCells[i]
            if (player == COMPUTER) {
                placeMove(cell, COMPUTER)
                val currentScore = minimax(depth + 1, PLAYER)
                max = max(currentScore, max)

                if (currentScore >= 0) {
                    if (depth == 0) computersMove = cell
                }

                if (currentScore == 1) {
                    g.gameBoard[cell.row][cell.col] = ""
                    break
                }

                if (i == g.availableCells.size - 1 && max < 0) {
                    if (depth == 0) computersMove = cell
                }

            } else if (player == PLAYER) {
                placeMove(cell, PLAYER)
                val currentScore = minimax(depth + 1, COMPUTER)
                min = min(currentScore, min)

                if (min == -1) {
                    g.gameBoard[cell.row][cell.col] = ""
                    break
                }
            }
            g.gameBoard[cell.row][cell.col] = ""
        }
        return if (player == COMPUTER) max else min
    }
}

class GamePlay {
    val gameBoard = Array(3) { arrayOfNulls<String>(3) }
    val availableCells: List<Cell>
        get() {
            val cells = mutableListOf<Cell>()
            for (row in gameBoard.indices) {
                for (col in gameBoard.indices) {
                    if (gameBoard[row][col].isNullOrEmpty()) {
                        cells.add(Cell(row, col))
                    }
                }
            }
            return cells
        }

    val isGameOver: Boolean
        get() = hasComputerWon() || hasPlayerWon() || availableCells.isEmpty()

    fun hasComputerWon(): Boolean {
        if (gameBoard[0][0] == gameBoard[1][1] && gameBoard[0][0] == gameBoard[2][2] && gameBoard[0][0] == TicTacToe.COMPUTER) {
            return true
        }

        if(gameBoard[0][2] == gameBoard[1][1] && gameBoard[0][2] == gameBoard[2][0] && gameBoard[0][2] == TicTacToe.COMPUTER) {
            return true
        }

        for (i in gameBoard.indices) {
            if (gameBoard[i][0] == gameBoard[i][1] && gameBoard[i][0] == gameBoard[i][2] && gameBoard[i][0] == TicTacToe.COMPUTER) {
                return true
            }
            if(gameBoard[0][i] == gameBoard[1][i] && gameBoard[0][i] == gameBoard[2][i] && gameBoard[0][i] == TicTacToe.COMPUTER) {
                return true
            }
        }
        return false
    }

    fun hasPlayerWon(): Boolean {
        if (gameBoard[0][0] == gameBoard[1][1] && gameBoard[0][0] == gameBoard[2][2] && gameBoard[0][0] == TicTacToe.PLAYER) {

            return true
        }
        if(gameBoard[0][2] == gameBoard[1][1] && gameBoard[0][2] == gameBoard[2][0] && gameBoard[0][2] == TicTacToe.PLAYER) {

            return true
        }

        for (i in gameBoard.indices) {
            if (gameBoard[i][0] == gameBoard[i][1] && gameBoard[i][0] == gameBoard[i][2] && gameBoard[i][0] == TicTacToe.PLAYER) {
                return true
            }
            if(gameBoard[0][i] == gameBoard[1][i] && gameBoard[0][i] == gameBoard[2][i] && gameBoard[0][i] == TicTacToe.PLAYER) {
                return true
            }
        }
        return false
    }
}