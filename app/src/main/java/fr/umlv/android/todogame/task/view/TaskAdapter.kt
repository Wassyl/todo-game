package fr.umlv.android.todogame.task.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import fr.umlv.android.todogame.R
import fr.umlv.android.todogame.database.entityclass.Task

class TaskAdapter(private val tasks:List<Task>): RecyclerView.Adapter<TaskViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.fragment_task_item, parent, false)
        return TaskViewHolder(view)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.updateQuest(tasks[position])
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

}