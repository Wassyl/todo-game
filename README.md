# ESIPE - 2020/2021

Projet réalisé dans le cadre de l'UE de programmation au semestre 4, cours d'Interfaces Graphiques.

# Structure du projet

```
____
    |____ database - Gestion de la base de données
    |
    |____ avatar - Gestion de l'inventaire du joueur
    |
    |____ minigames - Minis-jeux
    |
    |____ quest - Afficher les tâches en cours 
    |
    |____ dungeon - Partie jeu en 2D d'exploration avec des salles
    |
    |_ MainActivity.java - Menu principal pour accéder aux autres parties du programme
```

### quest - Tâches du joueur

Affichage de la liste de quêtes avec une RecyclerView.

### database - Base de données - SQLite

SQLite fut choisit avec SQLiteOpenHelper, le classe DataBase.kt (package database) en hérite et offre un design pattern basé sur le Singleton pour éviter des soucis de performances. Ceci permet l'intéraction avec l'intégralité du projet.

Les données de stockage sont spécifiés dans le package database.entityclass

Nous avions essayé d'utiliser Room sans succès. 

### dungeon - Carte 2D et parcours de salles

Actuellement, nous pouvons afficher un joueur et le faire déplacer sur une carte. Un ennemi est présent et nous pouvons le toucher. Le lancement d'un mini-jeu n'est pas encore en place.

### memory - Jeu de mémoire

Mini-jeu - Nicolas PIETRAC

Jeu de mémoire où il faut trouver les paires de cartes qui sont face cachées

### tictactoe - Jeu du morpion

Mini-jeu Cécile YEN

Jeu du morpion. Alignez trois symboles pour gagner.

### doggobonkgame - Tapez le chien pour gagner

Mini-jeu - Wassyl EL HACHIMI

Appuyez vite sur la tête du chien avant que le compte à rebours soit finit.
Si vous avez assez de points, vous gagnez la partie.

# Enoncé du projet TodoGame
L'objectif de ce projet est de réaliser une application Android implantant un gestionnaire de tâches proposant une approche ludique afin de les réaliser. Chaque tâche accomplie donne le droit d'explorer des salles à la recherche de récompenses avec notamment des combats ludiques contre des adversaires.

# Principe

Le projet met en œuvre un bâtiment fictif comportant un niveau avec différentes salles de part et d'autre d'un long corridor. Chacune de ses salles porte un nom qui correspond à une catégorie de tâche que l'on souhaite réaliser (par exemple des matières universitaires à étudier). Les salles sont ensuite représentées sous la forme de rectangles posés de part et d'autre du corridor (avec une porte communiquant du corridor vers la salle). Une salle peut comporter des monstres et des artéfacts à récupérer. Un monstre est un adversaire qui propose un défi par l'intermédiaire d'un jeu. Ce monstre peut fournir lui-même un ou plusieurs artéfacts lorsqu'il est battu. Un artéfact est un objet conservé dans l'inventaire du joueur et pouvant conférer certaines propriétés lorsqu'il est utilisé. On pourra définir pour certains artéfacts une date d'expiration : ils disparaîtront de l'inventaire s'ils ne sont pas utilisés.

En plus des salles pour chaque catégorie définie par l'utilisateur, il existe une salle appelée cusine. Celle-ci permet, en mélangeant un ou plusieurs artéfacts de l'inventaire, d'obtenir de nouveaux artéfacts selon certaines règles que l'on définira.

L'utilisateur peut définir lui-même les différentes catégories de tâches (auxquelles sont associées une salle). Il peut ajouter ou supprimer une catégorie à tout moment.


# Le gestionnaire de tâches
Le gestionnaire de tâches propose une interface permettant d'ajouter ou supprimer des tâches dans une catégorie. Une tâche représente une action que l'utilisateur s'impose à réaliser.


# Echéances de tâches et notification
Il est possible d'associer une échéance à une tâche. Si l'utilisateur ne respecte pas l'échéance spécifiée pour la réalisation de la tâche, l'application peut déclencher une notification de rappel.


# Suivi de réalisation de la tâche
L'utilisateur peut indiquer lorsqu'il commence à travailler sur une tâche. Un chronomètre est alors affiché en notification indiquant que la tâche est en cours de réalisation. Lorsque l'utilisateur a terminé sa session de travail. il le signale à l'application depuis la notification tout en indiquant le pourcentage actuel de réalisation de la tâche ainsi qu'un commentaire facultatif. Toutes les sessions de travail sont journalisées et il est possible d'afficher leur détail pour chaque tâche ainsi qu'un diagramme indiquant la progression reportée de la tâche au cours du temps.

Il est possible également d'associer à une session de travail, en plus d'une commentaire, un message sonore enregistré ou une note manuscrite.

Lorsqu'une tâche est terminée, la porte de la salle liée à la catégorie de la tâche se déverrouille : il devient ainsi possible de l'explorer.


# L'exploration des salles
Un plan en 2D (avec vue de dessus) permet d'explorer les salles. L'utilisateur est représenté par un petit avatar sur l'écran qui se déplace en ligne droite de sa position actuelle à une nouvelle position touchée tout en faisant défiler au besoin la carte.

Initialement l'avatar se déplace dans le couloir : les salles déverouillées par accomplissement de tâches sont affichées différemment pour les distinguer des salles verrouillées. Le nom des salles (nom de catégorie) est également indiqué.

L'avatar peut entrer dans une salle déverrouillée en passant par la porte. Une fois entré, on peut visualiser des artéfacts positionnés à des emplacements aléatoires ainsi que des monstres, monstres qui peuvent être figés mais également mobiles et poursuivre l'avatar. Chaque exploration de salle donne lieu au positionnement de nouveaux artéfacts et de monstre.

Lorsque l'utilisateur rencontre un artéfact, celui-ci est ajouté dans son inventaire. Lorsqu'un monstre est rencontré, celui-ci impose à l'utilisateur de se confronter à un jeu qui aura certaines conséquences sur l'avatar.


# L'avatar
L'avatar est représenté graphiquement sur l'écran par une petite icône mais il est également possible d'avoir une représentation visuelle en plein écran de celui-ci. Il possède un visage ainsi qu'un accoutrement qui peut varier en fonction de caractéristiques évolutives. Par exemple un artéfact stocké dans l'inventaire peut être un vêtement que l'on peut appliquer à l'avatar (un chapeau, des lunettes...).

L'avatar possède aussi différentes jauges (le développeur pourra mettre en oeuvre les jauges qu'il souhaite, e.g. jauge de vitalité, jauge d'expérience, jauge d'attaque, jauge de défense...). Le déroulement des jeux entrepris ainsi que l'utilisation d'un artéfact de l'inventaire est susceptible d'avoir une influence sur une ou plusieurs de ces jauges. Le temps qui passe peut également avoir une action sur des jauges (par exemple diminution de la jauge de vitalité qui peut être remplie par utilisation d'un artéfact nourriture).


# Les jeux
Il est demandé d'implanter trois jeux différents sous la forme d'applications externes qui seront lancées lorsque l'on aborde un monstre. Les activités de ces jeux seront lancées par startActivityForResult.

Une totale liberté est accordée quant à la nature de ces jeux (jeu d'arcade, de réflexion...). Il est toutefois demandé que ceux-ci fassent plutôt appel aux qualités d'adresse et de perspicacité de l'utilisateur plutôt qu'au hasard. Il n'est pas interdit d'utiliser des communications réseau au sein des jeux.

Si le projet dans son ensemble doit être un travail collectif du groupe, l'implantation des jeux échappe à cette règle. Chaque membre du groupe doit réaliser individuellement un jeu en tant qu'application séparée.

L'application n'a pas une connaissance préalable des jeux installés, elle essaiera de sélectionner à tour de rôle un des jeux à chaque monstre rencontré (elle peut interroger le système pour récupérer la liste des jeux installés).


# Les mondes et leur sauvegarde/restauration
Un monde représente l'ensemble de l'état des tâches et de l'avatar avec ses caractéristiques (inventaire, jauges). Par défaut, l'application gère un seul monde mais il est possible de créer d'autres mondes (ou d'en supprimer) si l'utilisateur souhaite par exemple cloisonner des tâches personnelles et professionnelles ou alors si plusieurs utilisateurs se partagent le même appareil. Il doit être possible de passer facilement d'un montre à l'autre.

Il est également demandé de pouvoir sauvegarder un monde à un instant donné sous la forme d'un fichier (dont vous choisirez le format) ainsi que de le restaurer à partir du fichier. On pourrait aussi envoyer un monde par différents moyens (email, logiciel de messagerie...). On peut ainsi reprendre un monde d'un appareil à un autre appareil.


# Développement
Le développement devra être réalisé en utilisant un dépôt versionné qui devra être rendu en temps utile. Une documentation de développement décrivant l'architecture du projet, l'organisation du travail, les difficultés rencontrées et les solutions apportées devra être réalisée et rendue au format PDF. Un fichier README doit également être présent à la racine du répertoire du projet pour le décrire succinctement ainsi qu'une documentation PDF pour l'utilisateur. Chaque jeu doit aussi faire l'objet d'une documentation PDF.

Bien sûr tout plagiat de code est formellement interdit. L'utilisation de bibliothèques externes (autres que les bibliothèques Android de base) est a priori interdite sauf pour les tâches de communication réseau (si cela est nécessaire) ou de sérialisation. Si une bibliothèque vous intéresse, le mieux est de demander l'autorisation par un message par email ou sur Discord.


# Fonctionnalités avancées
Il est possible d'implanter de nouvelles fonctionnalités lorsque celles demandées par l'énoncé sont mises en œuvres. En particulier il peut être intéressant de réfléchir a la possibilité d'interagir en réseau entre différents utilisateurs de l'application (jeux multi-joueurs, tâches collectives...). Il est possible aussi d'envisager que l'on puisse détecter automatiquement la satisfaction de certaines tâches (par exemple si une tâche consiste à réaliser des courses dans un magasin dont on connaît les coordonnées géographiques, on peut considérer la tâche satisfaite si l'utilisateur est présent un certain temps à cette position).
