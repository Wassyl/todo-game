package fr.umlv.android.todogame.category

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.umlv.android.todogame.R
import fr.umlv.android.todogame.database.entityclass.Category


class CategoryAdapter(private val cat:List<Category>, val intent: Intent, val context: Context, val listener: OnItemClickListener): RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.activity_list_item_category, parent, false)
        return CategoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val item = cat[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return cat.size
    }

    inner class CategoryViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var editCategory = itemView.findViewById<TextView>(R.id.editCategory)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            if(position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position,intent,context)
            }
        }

        fun bind(item: Category) {
            editCategory.text = item.title
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int, intent: Intent, context: Context)
    }

}

