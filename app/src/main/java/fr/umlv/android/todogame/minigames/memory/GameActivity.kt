package fr.umlv.android.todogame.minigames.memory
/* ALL IMAGES ARE FREE AND COME FROM https://svgsilh.com/fr/ */
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import fr.umlv.android.todogame.MemoryCard
import fr.umlv.android.todogame.R
import fr.umlv.android.todogame.R.drawable.*
import kotlinx.android.synthetic.main.tictactoe_main.*


class GameActivity : AppCompatActivity() {
    private var indexSingleCard: Int? = null //nullable int

    private lateinit var buttons: List<ImageButton>
    private lateinit var cards: List<MemoryCard>
    private var found: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.memory_main)

        val images = mutableListOf(ic_card1_cat, ic_card2_flower, ic_card3_bird, ic_card4_taxi)
        images.addAll(images)
        images.shuffle() //randomize the order in the list so you cant guess where the images are from the previous game !


        buttons = listOf(
            findViewById(R.id.imgBut1),
            findViewById(R.id.imgBut2),
            findViewById(R.id.imgBut3),
            findViewById(R.id.imgBut4),
            findViewById(R.id.imgBut5),
            findViewById(R.id.imgBut6),
            findViewById(R.id.imgBut7),
            findViewById(R.id.imgBut8)
        )

        cards = buttons.indices.map { index -> MemoryCard(images[index])
        }

        buttons.forEachIndexed { number, button ->
            button.setOnClickListener {
                updateModels(number)
                updateView()
            }
        }
    found = 0
    }

    private fun updateView() {
        cards.forEachIndexed { number, card ->
            val button = buttons[number]
            button.setImageResource(if (card.FacedUp) card.id else ic_verso_toutes_cartes)
        }
    }

    private fun updateModels(number: Int) {
        val card = cards[number]
        if (card.Matched) {
            return
        }
        //0card flipped = restore + flip selected
        //1card flipped = flip + check matches
        //2cards -> restore and flip
        if (indexSingleCard == null) {
            resetDisplay()
            indexSingleCard = number
        } else {
            isMatchingOrNot(indexSingleCard!!, number) //non null
            indexSingleCard = null
        }
        card.FacedUp = !card.FacedUp

    }

    private fun resetDisplay() {
        for (card in cards) {
            if (!card.Matched) {
                card.FacedUp = false
            }
        }
    }

    private fun isMatchingOrNot(firstCard: Int, secondCard: Int) {
        if (cards[firstCard].id == cards[secondCard].id){
            cards[firstCard].Matched = true
            cards[secondCard].Matched = true
            found++
            if (found == 4){
                val returnIntent = Intent()
                setResult(Activity.RESULT_OK, returnIntent)
                finish()
            }
        }
        else {
            Toast.makeText(this, "Loupé nulos", Toast.LENGTH_SHORT).show()
        }
    }


}