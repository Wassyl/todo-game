package fr.umlv.android.todogame.dungeon;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Random;

import fr.umlv.android.todogame.database.DataBase;
import fr.umlv.android.todogame.database.entityclass.User;

public class GameView extends SurfaceView implements Runnable {

    public static final String DUNGEON_ASSETS_LOCATION = "dungeon";

    public static int SCREEN_WIDTH;
    public static int SCREEN_HEIGHT;
    public static int SCREEN_DANGER_ZONE;

    private final GameActivity gameActivity;

    private final Player player;
    private final Enemy[] enemies;
    private final Chest[] chests;
    private final Bitmap background;

    private final Paint paint;

    final static int HITBOX_DESTINATION = 10;

    private boolean isPlaying = false;

    private Thread thread;

    private long deltaTime;
    private long lastTime;
    private long currentTime;

    public GameView(Context context, GameActivity gameActivity, int screenWidth, int screenHeight) {
        super(context);
        this.gameActivity = gameActivity;
        this.SCREEN_WIDTH = screenWidth;
        this.SCREEN_HEIGHT = screenHeight;
        this.SCREEN_DANGER_ZONE = screenHeight / 2;

        Random spawnRand = new Random();
        HashMap<String, Bitmap> assets = loadAllAssets(context);

        // Initialisation du joueur
        Bitmap bmPlayer = resizeBitmap(assets.get("player"),4,4);
        this.player = new Player(bmPlayer,4,3,SCREEN_WIDTH/2, 9 * SCREEN_HEIGHT/10);

        User user =  Build.VERSION.SDK_INT >= Build.VERSION_CODES.P ?  DataBase.Companion.getDataBaseInstance(context).getDefaultUser() : new User(0, "default", 0, 5);

        // Initialisation des artefacts
        this.chests = new Chest[user.getChestsAvailable()];
        for(int i = 0; i < chests.length; i++ ){
            this.chests[i] = new Chest(resizeBitmap(assets.get("chest"),4,4), 1 , 2, spawnRand.nextInt(SCREEN_WIDTH), spawnRand.nextInt(SCREEN_DANGER_ZONE));
        }

        // Initialisation des ennemies
        this.enemies = new Enemy[user.getEnemies()];
        for(int i = 0; i < enemies.length; i++ ){
            this.enemies[i] = new Enemy(resizeBitmap(assets.get(getRandomEnemyName()),4,4),4,3, spawnRand.nextInt(SCREEN_WIDTH), spawnRand.nextInt(SCREEN_DANGER_ZONE));
        }

        // Image de fond en bois
        this.background = resizeBitmap(assets.get("background"), 2, 2);

        // Effaçage
        paint = new Paint();
        paint.setColor(Color.WHITE);

        currentTime = 0;
        lastTime = System.currentTimeMillis();
    }

    private Bitmap resizeBitmap(Bitmap bitmap, float scaleHeight, float scaleWidth) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        int newWidth = (int)(width * scaleWidth);
        int newHeight = (int)(height * scaleHeight);

        return Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
    }

    /** Load all the sprites from the assets */
    private HashMap<String, Bitmap> loadAllAssets(Context context) {
        HashMap<String, Bitmap> map = new HashMap<>();
        try {
            map.put("background", BitmapFactory.decodeStream(context.getAssets().open(DUNGEON_ASSETS_LOCATION + File.separator + "background-wood.jpg")));

            map.put("player", BitmapFactory.decodeStream(context.getAssets().open(DUNGEON_ASSETS_LOCATION + File.separator + "player.png")));
            // Ennemies
            map.put("knight", BitmapFactory.decodeStream(context.getAssets().open(DUNGEON_ASSETS_LOCATION + File.separator + "knight.png")));
            map.put("frozen", BitmapFactory.decodeStream(context.getAssets().open(DUNGEON_ASSETS_LOCATION + File.separator + "frozen.png")));
            map.put("guard", BitmapFactory.decodeStream(context.getAssets().open(DUNGEON_ASSETS_LOCATION + File.separator + "guard.png")));
            map.put("cerbere", BitmapFactory.decodeStream(context.getAssets().open(DUNGEON_ASSETS_LOCATION + File.separator + "cerbere.png")));

            map.put("chest", BitmapFactory.decodeStream(context.getAssets().open(DUNGEON_ASSETS_LOCATION + File.separator + "chest.png")));
        } catch (IOException e) {
            Log.e(GameView.class.getName(), e.getMessage(), e);
        }
        return map;
    }

    @Override
    public void run() {
        while( isPlaying ) {
            update();
            draw();
        }
    }

    private void update() {
        deltaTime = System.currentTimeMillis() - lastTime;

        player.update(deltaTime, currentTime);
        for( Enemy enemy : enemies) {
            enemy.update(deltaTime, currentTime);
            enemy.followPlayer(player);
            if( Sprite.collides(player, enemy) ){
                gameActivity.startMiniGame();
                enemy.destroy();
                player.hide();
            }
        }

        for( Chest chest : chests) {
            if( Sprite.collides(player, chest) ){
                chest.getReward();
            }
        }

        currentTime += deltaTime;
        lastTime += deltaTime;
    }

    private void draw() {
        if( getHolder().getSurface().isValid() ){
            Canvas canvas = getHolder().lockCanvas();

            // EFFACAGE
            canvas.drawBitmap(background,0, 0, paint);

            // DESSIN DES PERSONNAGES
            player.draw(canvas);
            for (Sprite sprite : enemies) {
                sprite.draw(canvas);
            }
            // DESSIN DES ARTIFACTS
            for (Sprite sprite : chests) {
                sprite.draw(canvas);
            }

            getHolder().unlockCanvasAndPost(canvas);
        }
    }

    public void resume() {
        isPlaying = true;
        thread = new Thread(this);
        thread.start();
        player.show();
    }

    public void pause() {
        try {
            isPlaying = false;
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //
                break;
            case MotionEvent.ACTION_UP:
                player.updateVector(x, y);
                break;
        }
        return true;
    }

    private String getRandomEnemyName() {
        int nb = new Random().nextInt(4);
        switch (nb) {
            case 0: return "cerbere";
            case 1: return "frozen";
            case 2: return "guard";
            default: return "knight";
        }
    }
}
