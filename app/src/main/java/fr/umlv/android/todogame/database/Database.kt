package fr.umlv.android.todogame.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.os.Build
import androidx.annotation.RequiresApi
import fr.umlv.android.todogame.database.entityclass.Category
import fr.umlv.android.todogame.database.entityclass.Task
import fr.umlv.android.todogame.database.entityclass.User

@RequiresApi(Build.VERSION_CODES.P)
class DataBase(context: Context?) : SQLiteOpenHelper(context, "TodoGame", null, 3) {

    private val TABLE_USER = "User"
    private val TABLE_TASK = "Task"
    private val TABLE_CATEGORY = "Category"

    companion object {

        private var instance: DataBase? = null

        @Synchronized
        fun  getDataBaseInstance(context: Context): DataBase? {
            if (instance == null) {
                instance = DataBase(context.applicationContext)
            }
            return instance
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL("CREATE TABLE $TABLE_USER ( id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username TEXT NOT NULL, chestsAvailable INTEGER NOT NULL, enemies INTEGER NOT NULL)")
        db?.execSQL("CREATE TABLE $TABLE_TASK ( id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title TEXT NOT NULL, difficulty INTEGER NOT NULL, description TEXT, nameCategory TEXT NOT NULL)")
        db?.execSQL("CREATE TABLE $TABLE_CATEGORY ( id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title TEXT NOT NULL UNIQUE)")
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS $TABLE_USER")
        db?.execSQL("DROP TABLE IF EXISTS $TABLE_TASK")
        db?.execSQL("DROP TABLE IF EXISTS $TABLE_CATEGORY")
        onCreate(db)
    }

    fun getDefaultUser(): User? {
        val cursor = readableDatabase.rawQuery("SELECT * FROM $TABLE_USER WHERE username LIKE 'default';", null)
        if ( cursor.moveToFirst()) {
            return User(
                    cursor.getInt(0), // uid
                    cursor.getString(1), // username
                    cursor.getInt(2), // chestAvailable
                    cursor.getInt(3), // enemies
            )
        }
        return null
    }

    fun createDefaultUser(user : User) {
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put("username", user.username)
        cv.put("chestsAvailable", 2)
        cv.put("enemies", 2)
        db.insert(TABLE_USER, null, cv)
        db.close()
    }

    fun getQuests(nameCategory: String): List<Task> {
        val cursor = readableDatabase.rawQuery("SELECT * FROM $TABLE_TASK WHERE nameCategory= " + "'"+nameCategory+"'", null)
        val quests = ArrayList<Task>()

        if ( cursor.moveToFirst()) {
            do {
                val current = Task(
                        cursor.getInt(0), // id
                        cursor.getString(1), // Title
                        cursor.getFloat(2), // Difficulty
                        cursor.getString(3), // Description
                        cursor.getString(4), // nameCategory
                )
                quests.add(current)
            } while( cursor.moveToNext() )
        }

        cursor.close()
        return quests
    }

    fun insertQuest(quest: Task) {
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put("title", quest.title)
        cv.put("difficulty", quest.difficulty)
        cv.put("description", quest.description)
        cv.put("nameCategory", quest.nameCategory)
        db.insert(TABLE_TASK, null, cv)
        db.close()
    }

    fun insertCategory(category: Category) {
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put("title", category.title)
        db.insert(TABLE_CATEGORY, null, cv)
        db.close()
    }

    fun getCategory(): List<Category> {
        val cursor = readableDatabase.rawQuery("SELECT * FROM $TABLE_CATEGORY", null)
        val category = ArrayList<Category>()

        if (cursor.moveToFirst()) {
            do {
                val current = Category(
                    cursor.getInt(0), // id
                    cursor.getString(1), // Title
                )
                category.add(current)
            } while (cursor.moveToNext())
        }

        cursor.close()
        return category
    }

    // USELESS NOW
    fun getChestsAvailable(): Int {
        val cursor = readableDatabase.rawQuery("SELECT * FROM $TABLE_USER WHERE username LIKE 'default';", null)
        if ( cursor.moveToFirst()) {
            val nbChests = cursor.getInt(2)
            cursor.close()
            return nbChests //
        }
        return 0
    }

    fun updateChestsAvailable() {
        val db = this.writableDatabase
        db.execSQL("UPDATE $TABLE_USER SET chestsAvailable = IFNULL(chestsAvailable, 0) + 1 WHERE username LIKE 'default';")
        db.close()
    }

    fun updateEnemies() {
        val db = this.writableDatabase
        db.execSQL("UPDATE $TABLE_USER SET enemies = IFNULL(enemies, 0) + 1 WHERE username LIKE 'default';")
        db.close()
    }
    
}