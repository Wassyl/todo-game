package fr.umlv.android.todogame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import fr.umlv.android.todogame.database.DataBase;
import fr.umlv.android.todogame.database.entityclass.User;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        User user = Build.VERSION.SDK_INT >= Build.VERSION_CODES.P ?  DataBase.Companion.getDataBaseInstance(this).getDefaultUser() : null;
        if( user == null ) {
            user = createDefaultUser();
        }

        // Show user's name on the layout
        ((TextView) findViewById(R.id.tvUser)).setText( "Current User : " + user.getUsername() );

        Button startAvatar = findViewById(R.id.startAvatar);
        startAvatar.setOnClickListener( bt -> startActivity(new Intent(MainActivity.this, fr.umlv.android.todogame.avatar.GameActivity.class)));

        Button startCategory = findViewById(R.id.startCategory);
        startCategory.setOnClickListener( bt -> startActivity(new Intent(MainActivity.this, fr.umlv.android.todogame.category.RecyclerCategoryActivity.class)) );

        Button startWorldMap = findViewById(R.id.startDungeon);
        startWorldMap.setOnClickListener( bt -> startActivity(new Intent(MainActivity.this, fr.umlv.android.todogame.dungeon.GameActivity.class)) );
    }

    private User createDefaultUser() {
        User user = new User(0,"default", -1, -1);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            DataBase.Companion.getDataBaseInstance(this).createDefaultUser(user);
        }
        return user;
    }
}