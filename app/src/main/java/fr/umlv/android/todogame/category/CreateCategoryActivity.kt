package fr.umlv.android.todogame.category

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import androidx.annotation.RequiresApi
import com.google.android.material.floatingactionbutton.FloatingActionButton
import fr.umlv.android.todogame.R
import fr.umlv.android.todogame.database.DataBase
import fr.umlv.android.todogame.database.entityclass.Category

class CreateCategoryActivity : AppCompatActivity() {


    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_item_category)

        findViewById<FloatingActionButton>(R.id.valid).setOnClickListener {
            val category = Category(
                0,
                findViewById<EditText>(R.id.categories).text.toString(),
            )
            DataBase.getDataBaseInstance(applicationContext)?.insertCategory(category)
            val intent = Intent(this, RecyclerCategoryActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}