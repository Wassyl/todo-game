package fr.umlv.android.todogame.database.entityclass

import java.io.Serializable

data class User(
        val uid: Int,

        val username: String,
        val chestsAvailable: Int?,
        val enemies: Int?,
): Serializable