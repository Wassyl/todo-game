package fr.umlv.android.todogame.minigames.doggobonkgame;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import fr.umlv.android.todogame.R;

public class MainActivity extends AppCompatActivity {

    private static final int LAUNCH_SECOND_ACTIVITY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.doggo_main_activity);

        findViewById(R.id.startGame).setOnClickListener(b -> {
            startActivityForResult(new Intent(MainActivity.this, GameActivity.class), LAUNCH_SECOND_ACTIVITY);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Set result
        Intent returnIntent = new Intent();
        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            setResult( resultCode , returnIntent);
        }
        finish();
    }
}