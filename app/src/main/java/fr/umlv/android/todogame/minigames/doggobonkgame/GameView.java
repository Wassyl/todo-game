package fr.umlv.android.todogame.minigames.doggobonkgame;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Random;


public class GameView extends SurfaceView implements Runnable {

    private static class Background {

        private final Bitmap bitmap;

        private Background (Bitmap bm, int widthScreen, int heightScreen ) {
            bitmap = Bitmap.createScaledBitmap(bm, widthScreen, heightScreen, false);
        }

        private Bitmap getBitmap() {
            return bitmap;
        }
    }

    private class Point {
        private final int sizeX,sizeY;
        private final int bonus;
        private final int coolDownFactor;
        private final long lifeSpan;
        private final Bitmap image;

        private int x,y;
        private long lastAppearance;
        private boolean touched = false;
        private boolean inCoolDown = false;
        private long coolDown;

        private Point (Bitmap bitmap, int bonus, int lifeSpan, int cooldownFactor){
            this.image = bitmap;
            this.sizeX = bitmap.getWidth();
            this.sizeY = bitmap.getHeight();
            this.bonus = bonus;
            this.lifeSpan = lifeSpan;

            Random rand =  new Random();
            this.x = rand.nextInt(maxWidth-sizeX);
            this.y = rand.nextInt(maxHeight-sizeY);
            this.coolDown = rand.nextInt(500) * cooldownFactor;
            this.coolDownFactor = cooldownFactor;
            this.lastAppearance = System.currentTimeMillis();
        }

        private void collides(float x, float y) {
            if( this.x <= x && x <= this.x + sizeX && this.y <= y && y <= this.y + sizeY ) {
                touched = true;
            }
        }

        private void hide(){
            touched = false;
            inCoolDown = true;
            x = maxWidth;
        }

        private void update(){
            // Hidden
            if( inCoolDown ){
                coolDown -= deltaTime;
                if( coolDown <= 0 ) {
                    inCoolDown = false;
                    Random random = new Random();
                    x = random.nextInt(maxWidth-sizeX);
                    y = random.nextInt(maxHeight-sizeY) + marginScore;
                    coolDown = random.nextInt(500) * coolDownFactor;
                    lastAppearance = System.currentTimeMillis();
                }
                return;
            }
            // Visible
            if( System.currentTimeMillis() - lastAppearance >= lifeSpan ) {
                hide();
            }
        }
    }

    public static final String DOGGO_ASSETS_LOCATION = "doggobonkgame";

    private final int marginScore;
    private final int maxWidth;
    private final int maxHeight;
    private Thread thread;

    private final int minScore = 15;
    private final long maxTime = 15_000;
    private final GameActivity activity;

    private final Background background;
    private final Point doggo;
    private final Point[] maluses;
    private final Point bomb;
    private final Paint paint;
    private final SharedPreferences prefs;

    private final SoundPool soundPool;
    private final int soundBonk;
    private final int soundOof;

    private long deltaTime;
    private long lastTime;
    private long timer;
    private boolean isPlaying = false;
    private boolean gameOver = false;
    private boolean touchedBomb = false;
    private int points = 0;

    public GameView(GameActivity activity, int widthScreen, int heightScreen) {
        super(activity);
        this.activity = activity;

        marginScore = heightScreen / 10;
        maxWidth = widthScreen;
        maxHeight = heightScreen;

        HashMap<String, Bitmap> assets = loadAllAssets(activity);
        // get resources
        doggo = new Point(resizeBitmap(assets.get("doggo") , 2.5f ,2.5f), 2,600,1 );
        bomb = new Point(resizeBitmap(assets.get("bomb") , 0.3f ,0.3f), 0, 500, 5);

        this.maluses = new Point[4];
        for(int i =0; i<maluses.length; i++) {
            this.maluses[i] = new Point(resizeBitmap(assets.get("malus") , 3 ,3), -2,1000, 10);
        }

        paint = new Paint();
        paint.setTextSize(92);
        paint.setTextAlign(Paint.Align.CENTER);

        prefs = activity.getSharedPreferences("game", Context.MODE_PRIVATE);
        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        soundBonk = soundPool.load(activity, getResources().getIdentifier("bonk","raw", activity.getPackageName()), 1);
        soundOof = soundPool.load(activity, getResources().getIdentifier("oof","raw", activity.getPackageName()), 1);

        background = new Background(assets.get("background"), widthScreen, heightScreen);

        timer = 0;
        lastTime = System.currentTimeMillis();
    }

    /** Load all the sprites from the assets */
    private HashMap<String, Bitmap> loadAllAssets(Context context) {
        HashMap<String, Bitmap> map = new HashMap<>();
        try {
            map.put("background", BitmapFactory.decodeStream(context.getAssets().open(DOGGO_ASSETS_LOCATION + File.separator + "background.jpg")));
            map.put("bomb", BitmapFactory.decodeStream(context.getAssets().open(DOGGO_ASSETS_LOCATION + File.separator + "bomb.png")));
            map.put("doggo", BitmapFactory.decodeStream(context.getAssets().open(DOGGO_ASSETS_LOCATION + File.separator + "doggo.png")));
            map.put("malus", BitmapFactory.decodeStream(context.getAssets().open(DOGGO_ASSETS_LOCATION + File.separator + "malus.png")));
        } catch (IOException e) {
            Log.e(fr.umlv.android.todogame.dungeon.GameView.class.getName(), e.getMessage(), e);
        }
        return map;
    }

    private Bitmap resizeBitmap(Bitmap bitmap, float scaleHeight, float scaleWidth) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        int newWidth = (int)(width * scaleWidth);
        int newHeight = (int)(height * scaleHeight);

        return Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
    }

    @Override
    public void run() {
        while( isPlaying && !gameOver ) {
            update();
            draw();
        }
        if( points >= minScore && !touchedBomb ) {
            drawWin();
        } else {
            drawLose();
        }
        waitExit();
    }

    private void update(){
        deltaTime = System.currentTimeMillis() - lastTime;
        if( timer > maxTime ) {
            gameOver = true;
        }

        doggo.update();
        bomb.update();

        for(Point malus : maluses) {
            malus.update();
        }

        // Doggo
        if( doggo.touched ) {
            if (!prefs.getBoolean("isMute", false))
                soundPool.play(soundBonk, 1, 1, 0, 0, 1);
            points += doggo.bonus;
            doggo.hide();
        }

        for(Point malus : maluses) {
            if( malus.touched ) {
                if (!prefs.getBoolean("isMute", false))
                    soundPool.play(soundOof, 1, 1, 0, 0, 1);
                points += malus.bonus;
                malus.hide();
            }
        }

        if( bomb.touched ) {
            if (!prefs.getBoolean("isMute", false))
                soundPool.play(soundOof, 1, 1, 0, 0, 1);
            gameOver = true;
            touchedBomb = true;
            bomb.hide();
        }

        timer += deltaTime;
        lastTime += deltaTime;
    }

    private void draw(){
        if( getHolder().getSurface().isValid() ){
            Canvas canvas = getHolder().lockCanvas();

            // BACKGROUND
            canvas.drawBitmap(background.getBitmap(), 0, 0, paint);

            // AFFICHAGE SCORE
            paint.setColor(Color.WHITE);
            String currentScore = Integer.toString(points);

            long remainingTime = maxTime - timer;
            if( remainingTime < 10_000 ) {
                canvas.drawText("Temps restant : " + String.valueOf(remainingTime).substring(0, 1), maxWidth/2, (maxHeight - paint.ascent())/12, paint);
            } else {
                canvas.drawText("Temps restant : " + String.valueOf(remainingTime).substring(0, 2), maxWidth/2, (maxHeight - paint.ascent())/12, paint);
            }

            canvas.drawText("Score : " + currentScore, maxWidth/2, (maxHeight - paint.ascent())/6, paint);

            canvas.drawBitmap(doggo.image,doggo.x,doggo.y, paint);

            for(Point malus : maluses) {
                canvas.drawBitmap(malus.image,malus.x,malus.y, paint);
            }

            canvas.drawBitmap(bomb.image,bomb.x,bomb.y, paint);

            getHolder().unlockCanvasAndPost(canvas);
        }
    }

    private void drawWin(){
        if( getHolder().getSurface().isValid() ){
            Canvas canvas = getHolder().lockCanvas();
            paint.setColor(Color.GREEN);
            canvas.drawRect(0,maxHeight/2 - paint.getTextSize()*2, maxWidth, maxHeight/2 + paint.getTextSize()*2, paint);
            paint.setColor(Color.WHITE);
            canvas.drawText("Victoire ! hihi", maxWidth/2, maxHeight/2, paint);
            getHolder().unlockCanvasAndPost(canvas);
        }
    }

    private void drawLose(){
        if( getHolder().getSurface().isValid() ){
            Canvas canvas = getHolder().lockCanvas();
            paint.setColor(Color.RED);
            canvas.drawRect(0,maxHeight/2 - (paint.getTextSize()*2), maxWidth, maxHeight/2 + (paint.getTextSize()*2), paint);
            paint.setColor(Color.WHITE);
            canvas.drawText("Perdu !", maxWidth/2, maxHeight/2, paint);
            getHolder().unlockCanvasAndPost(canvas);
        }
    }

    private void waitExit() {
        try{
            Thread.sleep(3000);
            activity.finishGame( points >= minScore && !touchedBomb );
        } catch( InterruptedException e){
            e.printStackTrace();
        }
    }

    public void pause() {
        try {
            isPlaying = false;
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void resume() {
        isPlaying = true;
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                float x = event.getX();
                float y = event.getY();

                doggo.collides(x,y);
                for(Point malus : maluses) {
                    malus.collides(x,y);
                }
                bomb.collides(x,y);
                break;
        }
        return true;
    }
}
