package fr.umlv.android.todogame

data class MemoryCard(val id: Int, var FacedUp: Boolean = false, var Matched: Boolean = false)
