package fr.umlv.android.todogame.task

import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.umlv.android.todogame.R
import fr.umlv.android.todogame.database.DataBase
import fr.umlv.android.todogame.task.view.TaskAdapter

class DisplayTaskActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_task)
        val intent = intent
        val category = intent.getStringExtra("category")

         var quests = if(category != null) {
                 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                     DataBase.getDataBaseInstance(applicationContext)?.getQuests(category)
                 } else {
                     TODO("VERSION.SDK_INT < P")
                 }
             } else emptyList()
        val questAdapter = quests?.let { TaskAdapter(it) }
        val recyclerView = findViewById<RecyclerView>(R.id.tasks_recycler_view)
        recyclerView.adapter = questAdapter
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL ,false)
    }
}