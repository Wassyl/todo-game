package fr.umlv.android.todogame.dungeon;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;

import java.util.Random;


class Enemy implements Sprite {
    private final Bitmap image;
    private final int colCount;
    private final int imageWidth;
    private final int imageHeight;

    private final int fps;
    private final Random rand = new Random();

    private final int spriteWidth;
    private final int spriteHeight;

    private final int boundedBoxX;
    private final int boundedBoxY;

    private final Bitmap[] topBottom;

    private int currentCol = 1;

    private long frameTime = 0;
    private int frameModifier = 1; // Pivot pour les frames du sprite

    private final Matrix rotator = new Matrix();
    private int x;
    private int y;
    private float vectorX = 0;
    private float vectorY = 0;
    private boolean canMove = false;
    private boolean isAlive = true;

    private double distanceToPosition = 0;
    private float angle = 0;

    private boolean followPlayer = false;

    private static final float VELOCITY = 0.2f;
    private final float aggressivity;

    Enemy(Bitmap image, int rowCount, int colCount, int x, int y){
        this.image = image;
        this.colCount = colCount;

        this.imageWidth = image.getWidth();
        this.imageHeight = image.getHeight();

        this.spriteWidth = imageWidth/colCount;
        this.spriteHeight = imageHeight/rowCount;

        this.boundedBoxX = spriteWidth/2;
        this.boundedBoxY = spriteHeight/2;

        this.x = x;
        this.y = y;

        this.fps = 1_000 / (colCount*2);

        this.aggressivity = rand.nextFloat();

        this.topBottom = new Bitmap[colCount];
        for( int i = 0; i < colCount ;i++){
            this.topBottom[i] = getSubImage(0, i);
        }
    }

    private Bitmap getSubImage(int row, int col){
        return Bitmap.createBitmap(image, col*spriteWidth, row*spriteHeight, spriteWidth, spriteHeight);
    }

    private void hide(){
        x = GameView.SCREEN_WIDTH + boundedBoxX;
    }

    void update(long deltaTime, long now) {
        if( !isAlive ){
            return;
        }
        // Update l'animation, changer la frame courante
        if( now > frameTime + fps ){
            frameTime = now;
            if( currentCol == 0 || currentCol == colCount - 1 ){
                frameModifier *= -1;
            }
            currentCol += frameModifier;
        }
        if( !canMove ){
            currentCol = 1;
            return;
        }

        // Move player
        float speed = VELOCITY * deltaTime;
        double distanceXtravelled = speed * vectorX / distanceToPosition;
        double distanceYtravelled = speed * vectorY / distanceToPosition;

        // get new position
        this.x += distanceXtravelled;
        this.y += distanceYtravelled;

        // Ne pas dépasser les bordures de l'écran
        if( this.x - boundedBoxX <= 0 ){
            x = boundedBoxX;
            vectorX = 0;
        }
        if( this.y - boundedBoxY <= 0 ){
            y = boundedBoxY;
            vectorY = 0;
        }
        if( this.x + boundedBoxX >= GameView.SCREEN_WIDTH ){
            x = GameView.SCREEN_WIDTH - boundedBoxX;
            vectorX = 0;
        }
        if( this.y + boundedBoxY  >= GameView.SCREEN_HEIGHT ){
            y = GameView.SCREEN_HEIGHT - boundedBoxY;
            vectorY = 0;
        }
    }

    void destroy() {
        isAlive = false;
        hide();
    }

    @Override
    public Rect getCollisionShape() {
        return new Rect(x - boundedBoxX, y - boundedBoxY, x + boundedBoxX, y + boundedBoxY);
    }

    @Override
    public void draw(Canvas canvas) {
        rotator.reset();
        rotator.postRotate( angle - 90, boundedBoxX, boundedBoxY);
        rotator.postTranslate(x - boundedBoxX, y - boundedBoxY);
        canvas.drawBitmap(topBottom[currentCol], rotator, null);
    }

    @Override
    public void updateVector(float destX, float destY){
        canMove = true;
        vectorX = destX - x;
        vectorY = destY - y;

        angle = (float) Math.toDegrees(Math.atan2(vectorY,vectorX));
        distanceToPosition = Math.sqrt(vectorX*vectorX + vectorY*vectorY);
    }

    public void followPlayer(Player player) {
        if( !followPlayer ) {
            followPlayer = rand.nextFloat() <= aggressivity;
        } else {
            updateVector(player.getX(), player.getY());
        }
    }
}