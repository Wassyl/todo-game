package fr.umlv.android.todogame.category

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import fr.umlv.android.todogame.R
import fr.umlv.android.todogame.database.DataBase
import fr.umlv.android.todogame.database.entityclass.Category
import fr.umlv.android.todogame.task.CreateTaskActivity
import fr.umlv.android.todogame.task.DisplayTaskActivity

class RecyclerCategoryActivity : AppCompatActivity(), CategoryAdapter.OnItemClickListener {

    private var category : List<Category>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_category)
        val displayQuestIntent =  Intent(this, DisplayTaskActivity::class.java)
       category = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
           DataBase.getDataBaseInstance(applicationContext)?.getCategory()
       } else {
           TODO("VERSION.SDK_INT < P")
       }
        val categoryAdapter = category?.let {CategoryAdapter(it, displayQuestIntent, this, this)}
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewCategory)
        recyclerView.adapter= categoryAdapter
        recyclerView.layoutManager= LinearLayoutManager(this, LinearLayoutManager.VERTICAL ,false)

        findViewById<FloatingActionButton>(R.id.addCategory).setOnClickListener {
            val intent = Intent(this, CreateCategoryActivity::class.java)
            startActivity(intent)
            finish()
        }

        findViewById<FloatingActionButton>(R.id.btStartCreatQuestActivity).setOnClickListener {
            val intent = Intent(this, CreateTaskActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onItemClick(position: Int, intent: Intent, context: Context) {
        val clickedItem = category?.get(position)
        intent.putExtra("category", clickedItem!!.title)
        context.startActivity(intent)
    }
}