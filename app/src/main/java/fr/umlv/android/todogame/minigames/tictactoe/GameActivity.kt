package fr.umlv.android.todogame.minigames.tictactoe

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.GridLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import fr.umlv.android.todogame.R


class GameActivity : AppCompatActivity() {

    private val boardCells = Array(3) { arrayOfNulls<ImageView>(3) }

    var board = TicTacToe()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tictactoe_main)
        setBoard()
    }

    private fun setBoard() {
        val layoutBoard: GridLayout = findViewById(R.id.layoutBoard)
        for (i in boardCells.indices) {
            for (j in boardCells.indices) {
                boardCells[i][j] = ImageView(this)
                boardCells[i][j]?.layoutParams = GridLayout.LayoutParams().apply {
                    rowSpec = GridLayout.spec(i)
                    columnSpec = GridLayout.spec(j)
                    width = 300
                    height = 300
                    bottomMargin = 5
                    topMargin = 5
                    leftMargin = 5
                    rightMargin = 5
                }
                boardCells[i][j]?.setBackgroundColor(ContextCompat.getColor(this, R.color.grey))
                boardCells[i][j]?.setOnClickListener(CellClickListener(i, j))
                layoutBoard.addView(boardCells[i][j])
            }
        }
    }

    fun drawXO() {
        for (row in board.g.gameBoard.indices) {
            for (col in board.g.gameBoard.indices) {
                when (board.g.gameBoard[row][col]) {
                    TicTacToe.PLAYER -> {
                        boardCells[row][col]?.setImageResource(R.drawable.lunala)
                        boardCells[row][col]?.isEnabled = false
                    }
                    TicTacToe.COMPUTER -> {
                        boardCells[row][col]?.setImageResource(R.drawable.helios)
                        boardCells[row][col]?.isEnabled = false
                    }
                    else -> {
                        boardCells[row][col]?.setImageResource(0)
                        boardCells[row][col]?.isEnabled = true
                    }
                }
            }
        }
    }

    inner class CellClickListener(
        private val row: Int,
        private val col: Int,
        private val result: TextView = findViewById(R.id.result),
    ) : View.OnClickListener {

        override fun onClick(p0: View?) {

            if (!board.g.isGameOver) {
                val cell = Cell(row, col)
                board.placeMove(cell, TicTacToe.PLAYER)
                board.minimax(0, TicTacToe.COMPUTER)
                board.computersMove?.let {
                    board.placeMove(it, TicTacToe.COMPUTER)
                }
                drawXO()
            }

           val returnIntent = Intent()
            when {
                board.g.hasComputerWon() -> {
                    result.text = "The Computer Won"
                setResult(Activity.RESULT_CANCELED, returnIntent)
                finish()
                }
                board.g.hasPlayerWon() -> {
                    result.text = "Congrats ! You Won"
                    setResult(Activity.RESULT_OK, returnIntent)
                    finish()
                }
                board.g.isGameOver -> {
                    result.text = "Tie !!"
                    setResult(Activity.RESULT_CANCELED, returnIntent)
                    finish()
                }
            }
        }

    }
}