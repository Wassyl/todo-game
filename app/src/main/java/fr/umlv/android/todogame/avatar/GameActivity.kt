package fr.umlv.android.todogame.avatar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import fr.umlv.android.todogame.R
import fr.umlv.android.todogame.R.drawable.*

class GameActivity : AppCompatActivity() {

    private lateinit var hatImg: ImageButton
    private lateinit var shirtImg: ImageButton
    private lateinit var shoesBtn: ImageButton
    private lateinit var nextHat: ImageButton
    private lateinit var nextShirt: ImageButton
    private lateinit var nextShoes: ImageButton
    private lateinit var validateBt: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_avatar)

        val imgChapo = mutableListOf(ic_casquette, ic__noel, ic_sorcier)
        val imgTshirt = mutableListOf(ic_tshirt, ic_robe, ic_poncho)
        val imgChauss = mutableListOf(ic_basket, ic_bottes, ic_talons)

        validateBt = findViewById(R.id.validateBtn) //validate button
        hatImg = findViewById(R.id.hatImgBt)
        shirtImg = findViewById(R.id.shirtImgBt)
        shoesBtn = findViewById(R.id.shoesImgBt)

        nextHat = findViewById(R.id.nextHatBt)
        nextShirt = findViewById(R.id.nextShirtBt)
        nextShoes = findViewById(R.id.nextShoesBt)

        var iHat = 0
        var iShirt = 0
        var iBasket = 0

        nextHat.setOnClickListener {
            iHat = (iHat+1)%3
            hatImg.setImageResource(imgChapo.get(iHat))
        }


        nextShirt.setOnClickListener {
            iShirt = (iShirt+1)%3
            shirtImg.setImageResource(imgTshirt.get(iShirt))
        }

        nextShoes.setOnClickListener {
            iBasket = (iBasket+1)%3
            shoesBtn.setImageResource(imgChauss.get(iBasket))
        }

        validateBt.setOnClickListener {
            finish()
        }

    }
}