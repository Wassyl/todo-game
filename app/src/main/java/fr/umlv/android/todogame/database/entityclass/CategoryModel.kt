package fr.umlv.android.todogame.database.entityclass

import java.io.Serializable

data class Category(
    val qid: Int, // ID EN TABLE
    val title: String
) : Serializable {
    override fun toString() : String {
        return title
    }
}