package fr.umlv.android.todogame.task.view

import android.util.Log
import android.view.View
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.umlv.android.todogame.R
import fr.umlv.android.todogame.database.entityclass.Task

class TaskViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var tvCategory = itemView.findViewById<TextView>(R.id.tCategories)
    private var tvTitle = itemView.findViewById<TextView>(R.id.tTitle)
    private var tvDesc = itemView.findViewById<TextView>(R.id.tdesc)
    private var ratingBar = itemView.findViewById<RatingBar>(R.id.tdifficulty)

    fun updateQuest(quest: Task) {
        tvTitle.text = quest.title
        tvCategory.text = quest.nameCategory
        tvDesc.text = quest.description
        ratingBar.rating = quest.difficulty!!
    }
}