package fr.umlv.android.todogame.dungeon;

import android.graphics.Canvas;
import android.graphics.Rect;

interface Sprite {

    Rect getCollisionShape();

    void draw(Canvas canvas);

    void updateVector(float destX, float destY);

    static boolean collides(Sprite sprite1, Sprite sprite2) {
        return Rect.intersects(sprite1.getCollisionShape(),sprite2.getCollisionShape());
    }
}
