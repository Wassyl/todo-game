package fr.umlv.android.todogame.database.entityclass

import java.io.Serializable

data class Task(
        val qid: Int, // ID EN TABLE
        val title: String,
        val difficulty: Float?,
        val description: String,
        val nameCategory: String,
) : Serializable